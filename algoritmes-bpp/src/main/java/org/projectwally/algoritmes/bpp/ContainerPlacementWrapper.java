package org.projectwally.algoritmes.bpp;

import lombok.NonNull;
import lombok.Value;

import java.util.HashMap;
import java.util.Map;

@Value
public class ContainerPlacementWrapper {

    private final Afmeting3d<?> containerAfmeting;
    private final int containerId;

    private HashMap<RelativeContainerPosition<?>, Afmeting3d<?>> placedItems = new HashMap<>();

    /**
     * Probeert of het nieuwe item in deze container past.
     *
     * @return null als het item niet past, anders een RelativeContainerPosition waar het item geplaatst is.
     */
    public RelativeContainerPosition<?> place(@NonNull Afmeting3d<?> item) {
        RelativeContainerPosition<?> position = null;
        double offsetX = 0, offsetY = 0, offsetZ = 0;

        for (Map.Entry<RelativeContainerPosition<?>, Afmeting3d<?>> placedItem : placedItems.entrySet()) {
            Afmeting3d<?> placedItemBounds = placedItem.getValue();
            offsetX += placedItemBounds.getLengte().doubleValue();
            offsetY += placedItemBounds.getBreedte().doubleValue();
            offsetZ += placedItemBounds.getHoogte().doubleValue();
        }

        if (offsetX + item.getLengte().doubleValue() <= containerAfmeting.getLengte().doubleValue()) {
            position = new RelativeContainerPosition<>(containerId, offsetX, 0, 0);
        } else if (offsetY + item.getBreedte().doubleValue() <= containerAfmeting.getBreedte().doubleValue()) {
            position = new RelativeContainerPosition<>(containerId, 0, offsetY, 0);
        } else if (offsetZ + item.getHoogte().doubleValue() <= containerAfmeting.getHoogte().doubleValue()) {
            position = new RelativeContainerPosition<>(containerId, 0, 0, offsetZ);
        }

        if (position != null) {
            placedItems.put(position, item);
        }

        return position;
    }
}
