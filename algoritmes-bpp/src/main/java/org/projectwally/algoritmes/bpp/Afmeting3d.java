package org.projectwally.algoritmes.bpp;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Value;

/**
 * Representatie van een onveranderlijk driedimensionaal object,
 *     zoals de afmetingen van een tote (bin) of productverpakking.
 *
 * Om geheugen te besparen kunnen de velden meerdere integer types
 *     aannemen afhankelijk van de maximale afmetingen van het object.
 *     (e.g. byte voor kleine nummers, long voor grote nummers.)
 */
@Builder
@RequiredArgsConstructor
@Value
public class Afmeting3d<T extends Number> {

    private final T lengte, breedte, hoogte;

    /**
     * Bereken het volume van dit object in kubieke T (T³).
     *
     * @return volume in T³
     */
    public double getVolume() {
        return lengte.doubleValue()
               * breedte.doubleValue()
               * hoogte.doubleValue();
    }
}



