package org.projectwally.algoritmes.bpp.BestFit;
/*
// To use this algorithm, call BestFitAlgorithm(containers, binSizes).start()
// Where containers is an ArrayList<Container> and binSizes is an int[] with three values {Lenght, Width, Height}
// It returns an ArrayList<Bin> with the numbers of the bins and their respective items
// To print the result in the console, call bin.printResult() for each bin in the ArrayList<Bin>
//
// To make a container object, call Container(Length, Width, Height, Name)
// Where Length, Width and Height are the sizes of the container and Name is it's name
 */

import java.util.ArrayList;
import java.util.Collections;

public class BestFitAlgorithm {
    ArrayList<Container> containers;
    ArrayList<Bin> bins = new ArrayList<>();
    int binLength;
    int binWidth;
    int binHeight;

    public BestFitAlgorithm(ArrayList<Container> containers, int[] binDimensions) {
        this.containers = containers;
        this.binLength = binDimensions[0];
        this.binWidth = binDimensions[1];
        this.binHeight = binDimensions[2];
    }

    public ArrayList<Bin> start() {
        boolean fit;
        bins.clear();
        bins.add(new Bin(this.binLength, this.binWidth, this.binHeight, 0)); // Add first bin to array
        Collections.sort(containers); // Sort the containers by the volume in decreasing order

        // Check if all the items fit within the bounds of the bins
        for (Container container : containers) {
            if (container.getLength() > this.binLength
                    || container.getWidth() > this.binWidth
                    || container.getHeight() > this.binHeight) {
                System.out.println("Container " + container.getName() + " past niet in bins");
            }
        }

        for (Container container : containers) {
            fit = false;
            Collections.sort(bins); // Sort the bins by the volume thats left unused in increasing order

            for (Bin bin : bins) {

                if (bin.getVolumeLeft() - container.getVolume() >= 0) { // Check if there is enough volume to fit the container

                    if (bin.checkIfFits(container)) { // Check if container physically fits in bin
                        fit = true;
                        break; // Stop checking bins and move on to the next Container
                    }
                }

            }

            if (!fit) { // If the container doesn't fit in any opened bins, open a new bin and place the container in that bin
                int newId = bins.size();
                bins.add(new Bin(this.binLength, this.binWidth, this.binHeight, newId)); // Add new bin to array
                Bin newBin = bins.get(newId);
                newBin.addContainerToNewBin(container);
            }

        }
        return bins;
    }
}
