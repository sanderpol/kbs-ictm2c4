package org.projectwally.algoritmes.bpp.WorstFit;

public class RelativeContainerPosition {
    // Offset from the left bottom corner of the bin
    private final double lengthOffset, widthOffset, heightOffset;

    public RelativeContainerPosition(double lengthOffset, double widthOffset, double heightOffset) {
        this.lengthOffset = lengthOffset;
        this.widthOffset = widthOffset;
        this.heightOffset = heightOffset;
    }

    public double getLengthOffset() {
        return lengthOffset;
    }

    public double getWidthOffset() {
        return widthOffset;
    }

    public double getHeightOffset() {
        return heightOffset;
    }
}