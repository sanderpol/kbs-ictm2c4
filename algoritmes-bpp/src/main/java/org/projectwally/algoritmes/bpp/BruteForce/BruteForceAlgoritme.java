package org.projectwally.algoritmes.bpp.BruteForce;

import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.projectwally.algoritmes.bpp.Afmeting3d;
import org.projectwally.algoritmes.bpp.RelativeContainerPosition;
import org.projectwally.algoritmes.bpp.BinUtil;

import java.util.*;

@SuppressWarnings("ALL")
@UtilityClass
public class BruteForceAlgoritme {

    /**
     * Solve a Bin Packing Problem by checking every combination.
     *
     * @param toteSize the outer bounds size of a bin
     * @param items the items to place inside the bin
     * @return a map (dictionary) with the bounds of an item as the key
     *              and the bin number and relative position inside the bin as value
     *
     * @throws IllegalArgumentException if one of the input items does not fit inside the bin, or no input items are given.
     */
    @NonNull
    public static final <T extends Number, P extends Afmeting3d<T>>
            Map<P, RelativeContainerPosition<?>> bruteForce(@NonNull P toteSize, @NonNull Collection<? extends P> items) {
        // This is the best solution. It gets updated in the for-loop at the end of the function.
        long smallestSolutionRequiredBinsAmount = Long.MAX_VALUE;
        HashMap<P, RelativeContainerPosition<?>> smallestSolution = null;

        // Throw an error if there are no items to process
        if (items.size() <= 0) {
            throw new IllegalArgumentException("No items to process");
        }

        // Throw an error if an input item does not fit in a bin
        for (P item : items) {
            if (item.getLengte().doubleValue() > toteSize.getLengte().doubleValue()
                || item.getBreedte().doubleValue() > toteSize.getBreedte().doubleValue()
                || item.getHoogte().doubleValue() > toteSize.getHoogte().doubleValue()) {
                throw new IllegalArgumentException(String.format("Item %s does not fit in a bin (%s)", item, toteSize));
            }
        }

        // Iterate through all possible combinations
        for (List<? extends P> combinationItems : getCombinations(new ArrayList<>(items))) {
            // The current formation
            HashMap<P, RelativeContainerPosition<?>> currentSolution = new HashMap<>();
            // How many bins have been opened
            int binsOpened = 0;

            // Iterate through all items in the current combination
            for (P combinationItem : combinationItems) {
                // Iterate through all open bins
                for (int i = 0; i <= binsOpened; i++) {
                    // If the item fits inside the bin, calculate the relative position of the available spot.
                    RelativeContainerPosition<?> relativePosition = BinUtil.fitsInBin(toteSize, binsOpened, combinationItem,
                            BinUtil.getItemsInBin(binsOpened, (HashMap<Afmeting3d<?>, RelativeContainerPosition<?>>) currentSolution)
                                                                            .toArray(new Afmeting3d<?>[0]));

                    // If the item does not fit inside the bin, create a new bin and place the item in the left-bottom corner (on relative coordinate 0,0,0)
                    if (relativePosition == null) {
                        currentSolution.put(combinationItem, RelativeContainerPosition.builder()
                                                                                           .containerId(++binsOpened)
                                                                                           .lengteOffset(0).breedteOffset(0).hoogteOffset(0)
                                                                                           .build());
                        break;
                    }

                    // Place the item in the bin on the available spot
                    currentSolution.put(combinationItem, relativePosition);
                }
            }

            // If this solution is better than the existing solution, replace it.
            if (binsOpened < smallestSolutionRequiredBinsAmount) {
                smallestSolutionRequiredBinsAmount = binsOpened;
                smallestSolution = currentSolution;
            }
        }

        // Return de beste oplossing.
        return smallestSolution;
    }

    /**
     * Type-safe method to get a list of all possible combinations of the items in <i>items</i>.
     *
     * @param items list of input items
     * @return 2d list with all possible combinations.
     */
    @NonNull
    private static final <T> List<? extends List<T>> getCombinations(@NonNull List<T> items) {
        ArrayList<ArrayList<T>> output = new ArrayList<>();
        int itemsSize = items.size();

        for (int i = 0; i < itemsSize; i++) {
            ArrayList<T> newList = new ArrayList<>();
            newList.ensureCapacity(itemsSize);

            for (int j = 0; j < itemsSize; j++) {
                int oldIdx = i + j;
                if (oldIdx + 1 > itemsSize) {
                    oldIdx = oldIdx - itemsSize;
                }

                newList.add(items.get(oldIdx));
            }

            output.add(newList);
        }

        return output;
    }
}
