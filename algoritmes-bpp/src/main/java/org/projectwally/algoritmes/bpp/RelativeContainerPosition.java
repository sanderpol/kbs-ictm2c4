package org.projectwally.algoritmes.bpp;

import lombok.*;

/**
 * Gegevens die behoren tot de positie van een item in een doos.
 */
@Builder
@Value
public class RelativeContainerPosition<T extends Number> {

    /**
     * In welke tote (bin) een product komt.
     */
    private final long containerId;

    /**
     * De offset in T vanaf de starthoek van de verpakking.
     */
    private final T lengteOffset, breedteOffset, hoogteOffset;

}


