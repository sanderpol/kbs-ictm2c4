package org.projectwally.algoritmes.bpp.NextFit;

import lombok.NonNull;
import org.projectwally.algoritmes.bpp.Afmeting3d;
import org.projectwally.algoritmes.bpp.RelativeContainerPosition;
import org.projectwally.algoritmes.bpp.BinUtil;

import java.util.*;

@SuppressWarnings("ALL")
public class NextFitAlgoritme {

    /**
     * Solve a Bin Packing Problem by checking every combination.
     *
     * @param toteSize the outer bounds size of a bin
     * @param items the items to place inside the bin
     * @return a map (dictionary) with the bounds of an item as the key
     *              and the bin number and relative position inside the bin as value
     *
     * @throws IllegalArgumentException if one of the input items does not fit inside the bin, or no input items are given.
     */
    @NonNull
    public static final <T extends Number, P extends Afmeting3d<T>>
            Map<P, RelativeContainerPosition<?>> nextFit(@NonNull P toteSize, @NonNull Collection<? extends P> items) {
        // This is the best solution. It gets updated in the for-loop at the end of the function.
        HashMap<P, RelativeContainerPosition<?>> solution = new HashMap<>();
        int currentBin = 0;

        // Throw an error if there are no items to process
        if (items.size() <= 0) {
            throw new IllegalArgumentException("Geen items om te verwerken");
        }

        // Throw an error if an input item does not fit in a bin
        for (P item : items) {
            if (item.getLengte().doubleValue() > toteSize.getLengte().doubleValue()
                    || item.getBreedte().doubleValue() > toteSize.getBreedte().doubleValue()
                    || item.getHoogte().doubleValue() > toteSize.getHoogte().doubleValue()) {
                throw new IllegalArgumentException(String.format("Item %s does not fit inside a bin (%s)", item, toteSize));
            }
        }

        // Iterate through all items that need to be divided among the bins
        for (P item : items) {
            // If the item fits inside the current bin, calculate the relative position of the available spot ...
            RelativeContainerPosition<?> position = BinUtil.fitsInBin(toteSize, currentBin, item, BinUtil.getItemsInBin(currentBin, (HashMap<Afmeting3d<?>, RelativeContainerPosition<?>>) solution).toArray(new Afmeting3d<?>[0]));

            // ... and place it at that position in the current bin
            if (position != null) {
                solution.put(item, position);

            // If the item does not fit in the current bin, create a new bin and place the item in the lower left corner (at relative coords 0,0,0)
            } else {
                solution.put(item, RelativeContainerPosition.builder()
                                                                .containerId(++currentBin)
                                                                .lengteOffset(0).breedteOffset(0).hoogteOffset(0)
                                                                .build());
            }
        }

        // Return the solution.
        return solution;
    }
}
