package org.projectwally.algoritmes.bpp;

import lombok.NonNull;
import lombok.experimental.UtilityClass;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("FinalStaticMethod") // hoort zo.
@UtilityClass
public class BinUtil {

    /**
     * Get all items inside a bin from a formation mapping.
     *
     * @param binId the bin ID
     * @param items all items inside the formation
     * @return all items which reside inside the bin
     */
    @NonNull
    public static final Collection<Afmeting3d<?>> getItemsInBin(int binId, @NonNull HashMap<Afmeting3d<?>, RelativeContainerPosition<?>> items) {
        ArrayList<Afmeting3d<?>> result = new ArrayList<>();

        if (items != null) {
            for (Map.Entry<Afmeting3d<?>, RelativeContainerPosition<?>> item : items.entrySet()) {
                if (item.getValue().getContainerId() == binId) {
                    result.add(item.getKey());
                }
            }
        }

        return result;
    }

    /**
     * Checks of the <i>items</i> fit inside a bin.
     *
     * @param binSize the outer bounds of a bin
     * @param binId the ID of the bin
     * @param item the new item which needs to be placed
     * @param items the existing items in the bin
     * @return a RelativeContainerPosition if the item fits inside the bin, of null if it doesn't.
     */
    @SafeVarargs
    public static final <T extends Afmeting3d<?>> RelativeContainerPosition<?> fitsInBin(@NonNull T binSize, int binId, @NonNull T item, T... items) {
        // If there aren't any existing items, the new item always fits.
        if (items.length < 1) {
            return null;
        }

        ContainerPlacementWrapper placementWrapper = new ContainerPlacementWrapper(binSize, binId);

        // Place all existing items
        for (T iter : items) {
            placementWrapper.place(iter);
        }

        // Try to place the new item and return the result.
        return placementWrapper.place(item);
    }
}
