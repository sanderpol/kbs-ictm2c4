package org.projectwally.algoritmes.bpp.WorstFit;

public class Afmeting3d {

    private final double length, width, height;

    public double getVolume() {
        return length * width * height;
    }

    public Afmeting3d(double length, double width, double height) {
        this.length = length;
        this.width = width;
        this.height = height;
    }

    public double getLength() {
        return length;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }
}



