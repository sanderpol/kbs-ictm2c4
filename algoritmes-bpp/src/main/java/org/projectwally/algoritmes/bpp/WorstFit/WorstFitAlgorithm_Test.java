package org.projectwally.algoritmes.bpp.WorstFit;

import java.util.ArrayList;
import java.util.Arrays;

public class WorstFitAlgorithm_Test {
    public static void main(String[] args) {

        Container c1 = new Container(2, 2, 2, "Container 1");
        Container c2 = new Container(2, 2, 2, "Container 2");
        Container c3 = new Container(2, 2, 2, "Container 3");

        Container c4 = new Container(2, 2, 3, "Container 4");
        Container c5 = new Container(4, 2, 2, "Container 5");
        Container c6 = new Container(4, 2, 2, "Container 6");

        Container c7 = new Container(4, 2, 4, "Container 7");

        ArrayList<Container> containers = new ArrayList<>(Arrays.asList(c1, c2, c3, c4, c5, c6, c7));

        int[] binSizes = {4, 4, 4};
        ArrayList<Bin> indeling = new WorstFitAlgorithm(containers, binSizes).start();

        for (Bin bin : indeling) {
            bin.printResult();
        }

    }
}
