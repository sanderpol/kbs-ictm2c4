package org.projectwally.algoritmes.bpp.WorstFit;

public class Container extends Afmeting3d implements Comparable<Container> {
    private String name;
    private RelativeContainerPosition position;

    // Set the length, width and height of the container, rounding them to the nearest whole integer
    public Container(double length, double width, double height, String name) {
        super(Math.ceil(length), Math.ceil(width), Math.ceil(height));
        this.name = name;
    }

    public RelativeContainerPosition getPosition() {
        return position;
    }

    public void setPosition(RelativeContainerPosition position) {
        this.position = position;
    }

    @Override
    public int compareTo(Container container) {
        Double compareVolume = container.getVolume();
        Double thisVolume = this.getVolume();
        return (int) (compareVolume - thisVolume);
    }

    public String getName() {
        return name;
    }
}