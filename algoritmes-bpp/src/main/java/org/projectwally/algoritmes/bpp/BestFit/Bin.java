package org.projectwally.algoritmes.bpp.BestFit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Bin extends Afmeting3d implements Comparable<Bin> {

    private int binId;
    private Map<String, UnitCube> map = new HashMap<>();
    private ArrayList<String> mapKeys = new ArrayList<>();
    private ArrayList<Container> contents = new ArrayList<>();

    public Bin(double length, double width, double height, int binId) {
        super(length, width, height);
        this.binId = binId;
        createMap();
    }

    private void createMap() {
        double unit = 1; // Unit could be made adaptable later on

        // For each combination x, y and z within the bounds of the bin, add a UnitCube object to the Map and it's key to an ArrayList
        for (int x = 0; x < (this.getLength() / unit); x++) {
            for (int y = 0; y < (this.getWidth() / unit); y++) {
                for (int z = 0; z < (this.getHeight() / unit); z++) {
                    mapKeys.add("" + x + y + z);
                    map.put("" + x + y + z, new UnitCube(x, y, z));
                }
            }
        }
        Collections.sort(mapKeys); // Sort the keys from low to high
    }

    // If a new bin is opened, place the container at UnitCube '000'
    public void addContainerToNewBin(Container container) {
        addContainer(container, map.get("000"));
    }

    // Add new container to bin
    public void addContainer(Container container, UnitCube unitCube) {
        container.setPosition(new RelativeContainerPosition(unitCube.getX(), unitCube.getY(), unitCube.getZ())); // Set the relative position of the container to the left bottom corner

        // For each combination x, y and z within the bounds of the newly placed containter, set the UnitCube attribute occupied to true
        for (int x = unitCube.getX(); x < (unitCube.getX() + container.getLength()); x++) {
            for (int y = unitCube.getY(); y < (unitCube.getY() + container.getWidth()); y++) {
                for (int z = unitCube.getZ(); z < (unitCube.getZ() + container.getHeight()); z++) {
                    this.map.get("" + x + y + z).setOccupied(true);
                }
            }
        }

        contents.add(container); // Add container to the contents ArrayList
    }

    // Return the volume left in the Bin
    public double getVolumeLeft() {
        double volumeLeft = this.getVolume();
        for (Container container : contents) {
            volumeLeft -= container.getVolume();
        }

        return volumeLeft;
    }

    public int getBinId() {
        return binId;
    }

    @Override
    public int compareTo(Bin bin) {
        Double compareVolume = bin.getVolumeLeft();
        Double thisVolume = this.getVolumeLeft();
        return (int) (thisVolume - compareVolume);
    }

    public ArrayList<Container> getContents() {
        return contents;
    }

    // Check if testContainer fits in bin
    public boolean checkIfFits(Container testContainer) {
        for (String key : mapKeys) { // For each key in the ordered ArrayList of keys
            UnitCube unitCube = map.get(key);

            if (!unitCube.isOccupied()) { // Only move on if the UnitCube isn't occupied

                if (checkAvailability(unitCube, testContainer)) { // Check if the UnitCubes surrounding the current UnitCube are available
                    this.addContainer(testContainer, unitCube); // Add the container to the Bin
                    return true;
                }
            }
        }
        return false;
    }

    // Check if the UnitCubes surrounding the current UnitCube are available
    private boolean checkAvailability(UnitCube unitCube, Container testContainer) {
        // Check if the container fits within the bounds of the Bin when starting from the current UnitCube
        if (unitCube.getX() + testContainer.getLength() > this.getLength() ||
                unitCube.getY() + testContainer.getWidth() > this.getWidth() ||
                unitCube.getZ() + testContainer.getHeight() > this.getHeight()) {
            return false;
        }

        // For each UnitCube the container would be in, see if they are occupied
        for (int x = unitCube.getX(); x < (unitCube.getX() + testContainer.getLength()); x++) {
            for (int y = unitCube.getY(); y < (unitCube.getY() + testContainer.getWidth()); y++) {
                for (int z = unitCube.getZ(); z < (unitCube.getZ() + testContainer.getHeight()); z++) {
                    if (this.map.get("" + x + y + z).isOccupied()) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    // Print the binId and its contents
    public void printResult() {
        System.out.println("Containers in bin " + this.getBinId() + ":");
        for (Container container : this.getContents()) {
            System.out.println("    - " + container.getName() + " " + container.getPosition().getLengthOffset() + " " + container.getPosition().getWidthOffset() + " " + container.getPosition().getHeightOffset());
        }
        System.out.println("");
    }

}
