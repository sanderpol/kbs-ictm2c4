package org.projectwally.algoritmes.bpp.BestFit;

public class UnitCube {

    private boolean occupied = false;
    private int x;
    private int y;
    private int z;

    // Set the length, width and height of the UnitCube
    public UnitCube(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public boolean isOccupied() {
        return occupied;
    }

    public void setOccupied(boolean occupied) {
        this.occupied = occupied;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }
}
