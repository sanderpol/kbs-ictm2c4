package org.projectwally.algoritmes.random;

import org.junit.Assert;
import org.junit.Test;
import org.projectwally.algoritmes.bpp.Afmeting3d;
import org.projectwally.algoritmes.bpp.BruteForce.BruteForceAlgoritme;
import org.projectwally.algoritmes.bpp.NextFit.NextFitAlgoritme;
import org.projectwally.algoritmes.bpp.RelativeContainerPosition;

import java.lang.management.ManagementFactory;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

// Wordt niet aangeraden om op een PC met minder dan 64gb RAM te runnen... just sayin.
// Let op: Als er een OutOfMemoryException komt, dan stopt JUnit niet automatisch! Stuur een sigkill naar het proces.
// Allocate meer RAM voor de JVM in de build.gradle.
public class RandomBruteForceTest {

    private static final int                        ITERATIONS       = 5000;

    /**
     * The maximum outer bounds size of a bin.
     */
    private static final int                        MAX_BOUND        = 10;
    private static final Afmeting3d<Integer>        BIN_SIZE         = new Afmeting3d<>(MAX_BOUND, MAX_BOUND, MAX_BOUND);

    /**
     * The amount of items to create per problem.
     */
    private static final int                        ITEM_AMOUNT      = 5;

    /**
     * Testpopulations (refer to onderzoeksverslag for more info)
     */
    @SuppressWarnings("unchecked") // arrays kunnen niet type-safe worden aangemaakt
    private static final Set<Afmeting3d<Integer>>[] ITEMS_ARRAY      = new Set[ITERATIONS];

    private static final ThreadLocalRandom          RANDOM           = ThreadLocalRandom.current();

    /**
     * Solves a simple Bin Packing Problem with a few sample products.
     */
    @Test
    public void testRandomBruteForceSpeed() {
        // Check if we can measure the CPU usage time with JMX
        // If we would do this with System.nanoTime() it would be inaccurate because
        // other processes on the host computer can cause interferance
        Assert.assertTrue(ManagementFactory.getThreadMXBean().isCurrentThreadCpuTimeSupported());

        // Prepopulate the item array with random sample data.
        // This is done before we run the tests, because the generation of random numbers and objects
        // takes up time which we don't want to calculate in the test results.
        for (int i = 0; i < ITERATIONS - 1; i++) {
            // Create a new collection with 5 items (ITEM_AMOUNT = 5)
            ITEMS_ARRAY[i] = new HashSet<Afmeting3d<Integer>>() {{
                for (int j = 0; j < ITEM_AMOUNT; j++) {
                    add(new Afmeting3d<>(RANDOM.nextInt(MAX_BOUND), RANDOM.nextInt(MAX_BOUND), RANDOM.nextInt(MAX_BOUND)));
                }
            }};
        }

        // Register the start time
        long startTime = ManagementFactory.getThreadMXBean().getCurrentThreadCpuTime();

        // Execute 5000 calculations
        for (int i = 0; i < ITERATIONS - 2; i++) {
            BruteForceAlgoritme.bruteForce(BIN_SIZE, ITEMS_ARRAY[i]);
        }
        Map<Afmeting3d<Integer>, RelativeContainerPosition<?>> formation = BruteForceAlgoritme.bruteForce(BIN_SIZE, ITEMS_ARRAY[ITERATIONS - 2]);

        // Calculate the time it took to run
        long deltaTime = ManagementFactory.getThreadMXBean().getCurrentThreadCpuTime() - startTime;

        // Count the amount of required bins
        HashSet<Long> containerIds = new HashSet<>();
        for (Map.Entry<Afmeting3d<Integer>, RelativeContainerPosition<?>> bestFormationEntry : formation.entrySet()) {
            containerIds.add(bestFormationEntry.getValue().getContainerId());
        }

        // Print the results of the test
        System.err.printf("Random %dx Brute Force took %d ns (%d ms) and required %d bins to fit all items\n",
                          ITERATIONS, deltaTime, deltaTime / 1000000, containerIds.size());
    }
}
