package org.projectwally.algoritmes.random;

import org.junit.Assert;
import org.junit.Test;
import org.projectwally.algoritmes.bpp.BestFit.BestFitAlgorithm;
import org.projectwally.algoritmes.bpp.BestFit.Bin;
import org.projectwally.algoritmes.bpp.BestFit.Container;

import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

// Wordt niet aangeraden om op een PC met minder dan 64gb RAM te runnen... just sayin.
// Let op: Als er een OutOfMemoryException komt, dan stopt JUnit niet automatisch! Stuur een sigkill naar het proces.
// Allocate meer RAM voor de JVM in de build.gradle.
public class RandomBestFitTest {

    /**
     * The maximum outer bounds size of a bin.
     */
    private static final int                MAX_BOUND        = 10;

    /**
     * The amount of containers to create per problem.
     */
    private static final int                CONTAINER_AMOUNT = 5;

    private static final int                ITERATIONS       = 5000;
    private static final BestFitAlgorithm[] CONTAINERS_ARRAY = new BestFitAlgorithm[ITERATIONS];

    private static final ThreadLocalRandom  RANDOM_INSTANCE  = ThreadLocalRandom.current();

    /**
     * Solves a simple Bin Packing Problem with a few sample products.
     */
    @Test
    public void testRandomBestFitSpeed() {
        // Check if we can measure the CPU usage time with JMX
        // If we would do this with System.nanoTime() it would be inaccurate because
        // other processes on the host computer can cause interference
        Assert.assertTrue(ManagementFactory.getThreadMXBean().isCurrentThreadCpuTimeSupported());

        // Prepopulate the container array with random sample data.
        // This is done before we run the tests, because the generation of random numbers and objects
        // takes up time which we don't want to calculate in the test results.
        for (int i = 0; i < ITERATIONS; i++) {
            // Create a new collection with 5 products (CONTAINER_AMOUNT = 5)
            CONTAINERS_ARRAY[i] = new BestFitAlgorithm(new ArrayList<Container>() {{
                for (int j = 0; j < CONTAINER_AMOUNT; j++) {
                    add(new Container(RANDOM_INSTANCE.nextDouble(MAX_BOUND),
                                      RANDOM_INSTANCE.nextDouble(MAX_BOUND),
                                      RANDOM_INSTANCE.nextDouble(MAX_BOUND),
                                      ""));
                }
            }}, new int[] {MAX_BOUND, MAX_BOUND, MAX_BOUND});
        }

        // Register the start time
        long startTime = ManagementFactory.getThreadMXBean().getCurrentThreadCpuTime();

        // Execute 5000 calculations
        for (int i = 0; i < ITERATIONS - 1; i++) {
            CONTAINERS_ARRAY[i].start();
        }
        ArrayList<Bin> bins = CONTAINERS_ARRAY[ITERATIONS - 1].start();

        // Calculate the time it took to run
        long deltaTime = ManagementFactory.getThreadMXBean().getCurrentThreadCpuTime() - startTime;

        // Print the results of the test
        System.err.printf("Random %dx Best Fit took %d ns (%d ms) and required %d bins to fit all items\n",
                          ITERATIONS, deltaTime, deltaTime / 1000000, bins.size());
    }
}
