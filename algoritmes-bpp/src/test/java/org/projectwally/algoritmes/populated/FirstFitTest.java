package org.projectwally.algoritmes.populated;

import org.junit.Assert;
import org.junit.Test;
import org.projectwally.algoritmes.bpp.Afmeting3d;
import org.projectwally.algoritmes.bpp.FirstFit.FirstFitAlgoritme;
import org.projectwally.algoritmes.bpp.RelativeContainerPosition;

import java.lang.management.ManagementFactory;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class FirstFitTest {

    private static final int                      BIN_LENGTH    = 4,
                                                  BIN_WIDTH     = 7,
                                                  BIN_HEIGHT    = 7;

    /**
     * Testpopulation (refer to onderzoeksverslag for more info)
     */
    private static final Afmeting3d<Integer>      BIN_SIZE      = new Afmeting3d<>(BIN_LENGTH, BIN_WIDTH, BIN_HEIGHT);
    private static final Set<Afmeting3d<Integer>> ITEMS         = Collections.unmodifiableSet(new HashSet<Afmeting3d<Integer>>() {{
        add(new Afmeting3d<>(3, 2, 5));
        add(new Afmeting3d<>(1, 2, 3));
        add(new Afmeting3d<>(2, 1, 1));
        add(new Afmeting3d<>(4, 4, 3));
        add(new Afmeting3d<>(3, 3, 5));
    }});

    private static final int                      ITERATIONS    = 1000000;

    /**
     * Solves a simple Bin Packing Problem with a few sample products.
     */
    @Test
    public void testFirstFitSpeed() {
        // Check if we can measure the CPU usage time with JMX
        // If we would do this with System.nanoTime() it would be inaccurate because
        // other processes on the host computer can cause interferance
        Assert.assertTrue(ManagementFactory.getThreadMXBean().isCurrentThreadCpuTimeSupported());

        // Register the start time
        long startTime = ManagementFactory.getThreadMXBean().getCurrentThreadCpuTime();

        // Execute 1000000 calculations
        for (int i = 0; i < ITERATIONS - 1; i++) {
            FirstFitAlgoritme.firstFit(BIN_SIZE, ITEMS);
        }
        Map<Afmeting3d<Integer>, RelativeContainerPosition<?>> formation = FirstFitAlgoritme.firstFit(BIN_SIZE, ITEMS);

        // Calculate the time it took to run
        long deltaTime = ManagementFactory.getThreadMXBean().getCurrentThreadCpuTime() - startTime;

        // Count the amount of bins required
        HashSet<Long> containerIds = new HashSet<>();
        for (Map.Entry<Afmeting3d<Integer>, RelativeContainerPosition<?>> bestFormationEntry : formation.entrySet()) {
            containerIds.add(bestFormationEntry.getValue().getContainerId());
        }

        // Print the results of the test
        System.err.printf("%dx First Fit took %d ns (%d ms) en required %d bins to fit all items\n",
                          ITERATIONS, deltaTime, deltaTime / 1000000, containerIds.size());
    }
}
