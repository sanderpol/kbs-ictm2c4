package org.projectwally.algoritmes.populated;

import org.junit.Assert;
import org.junit.Test;
import org.projectwally.algoritmes.bpp.WorstFit.Bin;
import org.projectwally.algoritmes.bpp.WorstFit.Container;
import org.projectwally.algoritmes.bpp.WorstFit.WorstFitAlgorithm;

import java.lang.management.ManagementFactory;
import java.util.*;

public class WorstFitTest {

    private static final int                  BIN_LENGTH = 4,
                                              BIN_WIDTH  = 7,
                                              BIN_HEIGHT = 7;

    /**
     * Testpopulation (refer to onderzoeksverslag for more info)
     */
    private static final ArrayList<Container> CONTAINERS = new ArrayList<Container>() {{
        add(new Container(3, 2, 5, "1"));
        add(new Container(1, 2, 3, "2"));
        add(new Container(2, 1, 1, "3"));
        add(new Container(4, 4, 3, "4"));
        add(new Container(3, 3, 5, "5"));
    }};

    private static final WorstFitAlgorithm    INSTANCE   = new WorstFitAlgorithm(CONTAINERS, new int[] {BIN_LENGTH, BIN_WIDTH, BIN_HEIGHT});
    private static final int                  ITERATIONS = 100000;

    /**
     * Solves a simple Bin Packing Problem with a few sample products.
     */
    @Test
    public void testWorstFitSpeed() {
        // Check if we can measure the CPU usage time with JMX
        // If we would do this with System.nanoTime() it would be inaccurate because
        // other processes on the host computer can cause interferance
        Assert.assertTrue(ManagementFactory.getThreadMXBean().isCurrentThreadCpuTimeSupported());

        // Register the start time
        long startTime = ManagementFactory.getThreadMXBean().getCurrentThreadCpuTime();

        // Execute 1000000 calculations
        for (int i = 0; i < ITERATIONS - 1; i++) {
            INSTANCE.start();
        }
        ArrayList<Bin> bins = INSTANCE.start();

        // Calculate the time it took to run
        long deltaTime = ManagementFactory.getThreadMXBean().getCurrentThreadCpuTime() - startTime;

        // Print the results of the test
        System.err.printf("%dx Worst Fit took %d ns (%d ms) en required %d bins to fit all items\n",
                          ITERATIONS, deltaTime, deltaTime / 1000000, bins.size());
    }
}
