package org.projectwally.algoritmes;

import org.junit.Assert;
import org.junit.Test;
import org.projectwally.algoritmes.bpp.Afmeting3d;

/**
 * Tests om de functionaliteit van class Afmeting3d te testen.
 */
public class AfmetingTests {

    private static final short             LENGTE              = 4,
                                           BREEDTE             = 7,
                                           HOOGTE              = 19;
    private static final double            VERWACHTE_VOLUME    = LENGTE * BREEDTE * HOOGTE;

    private static final Afmeting3d<Short> TEST_OBJECT_CTOR    = new Afmeting3d<>(LENGTE, BREEDTE, HOOGTE),
                                           TEST_OBJECT_BUILDER = Afmeting3d.<Short>builder()
                                                                               .lengte(LENGTE)
                                                                               .breedte(BREEDTE)
                                                                               .hoogte(HOOGTE)
                                                                               .build();

    private static final Afmeting3d<?>[]   TEST_OBJECTS        = new Afmeting3d<?>[] {TEST_OBJECT_CTOR,
                                                                                      TEST_OBJECT_BUILDER};

    @Test
    public void testOfVolumeCorrectIs() {
        // Voor elk van de test objecten ...
        for (Afmeting3d<?> afmeting : TEST_OBJECTS) {
            // ... check of het verwacht volume overeenkomt met het aangegeven volume
            Assert.assertEquals(0, Double.compare(afmeting.getVolume(), VERWACHTE_VOLUME));
        }
    }
}
