package org.projectwally.algoritmes.tsp.DFSAlgoritme;

import org.projectwally.algoritmes.tsp.GraphImporter;
import org.projectwally.algoritmes.tsp.Node;

import java.util.ArrayList;
import java.util.Arrays;

public class DFSClass implements DFSInterface {

    private ArrayList<Node> nodes;
    private ArrayList<Integer>[] edges;
    private ArrayList<Node> returnNodes ;

    public void setNodes(ArrayList<Node> nodes) {
        this.nodes = nodes;
    }

    public void setEdges(ArrayList<Integer>[] edges) {
        this.edges = edges;
    }

    public void initReturnNodes(){
        if (this.returnNodes == null){
            this.returnNodes = new ArrayList<>();
        }
    }

    /**This Function traversals through graph using a depth first search.
     * This Function revers to another function witch is de body of this algorithm
     * the idea of een depth first search to go through nodes until there are no futher nodes on its path.
     * When that happens the algorithm goes back to where it could choose for multiple edges.
     * If all @targetNodesVisited are equal to true, then all values in the boolean array @visited are set to true.
     * @param targetNodes is the array of nodes that is being searched for.
     */
    public void DFSgetNode(ArrayList<Node> targetNodes) {
        boolean[] visited = new boolean[GraphImporter.NR_NODES];
        boolean[] targetNodesVisited = new boolean[targetNodes.size()];
        DFSRgetNode(GraphImporter.START_NODE, visited, targetNodes, targetNodesVisited);
        }

    /**
     * This method is a recursive method and because of this it calls up on it self, until all @TargetNodes have
     * been visited.
     * @param currentNode current node.
     * @param visited boolean with nodes that have been visited.
     * @param targetNodes is the array of nodes that is being searched for.
     * @param targetNodesVisited is the boolean array of nodes being searched for.
     */
    private void DFSRgetNode(int currentNode, boolean[] visited, ArrayList<Node> targetNodes, boolean[] targetNodesVisited) {

        // Set the current node to visited
        visited[currentNode] = true;

        //If the current node is the node searched for
        for (int i = 0; i < targetNodes.size(); i++) {
            if (nodes.get(currentNode).getValue() == targetNodes.get(i).getValue()) {
                targetNodesVisited[i] = true;
                returnNodes.add(nodes.get(currentNode));
                if (areAllTrue(targetNodesVisited)){
                    Arrays.fill(visited, true);
                }
            }
        }

        for (int j : edges[currentNode]) {
            //j is the next edge in the array
            //if the edge hasn't been visited
            if (!visited[j]) {
                DFSRgetNode(j, visited, targetNodes,targetNodesVisited);
            }
        }
    }

    public static boolean areAllTrue(boolean[] array){
        for (boolean b: array) {
            if(!b) return false;
        }
        return true;
    }

    public ArrayList<Node> getReturnNodes() {
        return this.returnNodes;
    }

    @Override
    public void setReturnNodesNull() {
        this.returnNodes = null;
    }

}
