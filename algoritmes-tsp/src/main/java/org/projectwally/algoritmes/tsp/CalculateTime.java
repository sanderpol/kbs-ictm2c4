package org.projectwally.algoritmes.tsp;

public class CalculateTime {

    public long duration;
    private long startTime;
    private long endTime;


    public void startTimer() {
        this.startTime = System.nanoTime();
    }

    public void endTimer() {
        this.endTime = System.nanoTime();
        duration = (endTime - startTime);
    }


}
