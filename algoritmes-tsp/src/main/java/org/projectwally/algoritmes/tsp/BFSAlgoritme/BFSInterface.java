package org.projectwally.algoritmes.tsp.BFSAlgoritme;

import org.projectwally.algoritmes.tsp.Node;

import java.util.ArrayList;

public interface BFSInterface {
    void BFSgetNode(ArrayList<Node> searchNodes);

    void setEdges(ArrayList<Integer>[] edges);

    void setNodes(ArrayList<Node> nodes);

    void initReturnNodes();


    ArrayList<Node> getReturnNodes();
}
