package org.projectwally.algoritmes.tsp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class GraphImporter {

    public final static int START_NODE = 1;
    public final static int NR_NODES = 1501;

    public static Graph getGraph() throws  Exception {

        String filename = "p_hat1500-3.clq.txt";

        System.err.println(new File(".").getCanonicalPath());
        FileInputStream fileInputStream = new FileInputStream(filename);
        InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
        BufferedReader reader = new BufferedReader(inputStreamReader);

        Graph graph = new Graph(NR_NODES);


        String line = reader.readLine();
        while (line != null) {
            switch (line.charAt(0)) {
                case 'e':
                    String[] split = line.split(" ");
                    int u = Integer.parseInt(split[1]);
                    int v = Integer.parseInt(split[2]);
                    graph.addBiDirectionalEdge(u,v);
                case 'c':
                    break;

            }
            line = reader.readLine();
        }

        return graph;
    }

}
