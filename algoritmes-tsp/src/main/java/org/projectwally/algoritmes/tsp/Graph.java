package org.projectwally.algoritmes.tsp;


import java.util.ArrayList;

public class Graph {
    public final static int NR_NODES = 26;
    public final static int START_NODE = 0;
    public ArrayList<Node> nodes;
    public ArrayList<Integer>[] edges;

    public Graph(int nodes) {
        this.edges = new ArrayList[nodes];
        this.nodes = new ArrayList<>();
        for (int i = 0; i < nodes; i++) {
            this.edges[i] = new ArrayList<>();
            this.nodes.add(new Node(i));
        }
    }

    public ArrayList<Node> getNodes() {
        return nodes;
    }

    public static Graph getGraph() {
        Graph graph = new Graph(NR_NODES);
        for (int i = 1; i < 9; i++) {
            graph.addBiDirectionalEdge(i, i + 1);
        }
        for (int i = 10; i < 16; i++) {
            graph.addBiDirectionalEdge(i, i + 1);
        }
        for (int i = 17; i < 21; i++) {
            graph.addBiDirectionalEdge(i, i + 1);
        }
        for (int i = 22; i < 24; i++) {
            graph.addBiDirectionalEdge(i, i + 1);
        }
        for (int i = 1; i < 5; i++) {
            graph.addBiDirectionalEdge(i, i + 9);
        }
        for (int i = 6; i < 13; i++) {
            graph.addBiDirectionalEdge(i, i + 7);
        }
        for (int i = 14; i < 19; i++) {
            graph.addBiDirectionalEdge(i, i + 5);
        }
        for (int i = 20; i < 23; i++) {
            graph.addBiDirectionalEdge(i, i + 3);
        }
        graph.addBiDirectionalEdge(0, 5);
        graph.addBiDirectionalEdge(5, 13);
        graph.addBiDirectionalEdge(13, 19);
        graph.addBiDirectionalEdge(19, 23);
        graph.addBiDirectionalEdge(23, 25);
        graph.addBiDirectionalEdge(24, 25);
        return graph;
    }

    public ArrayList<Integer>[] getEdges() {
        return edges;
    }


    public void setNode(Node n) {
        nodes.set(n.getIndex(), n);
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < edges.length; i++) {
            res.append(i).append(":");
            for (int j = 0; j < edges[i].size(); j++)
                res.append(" ").append(edges[i].get(j));
            if (i + 1 < edges.length)
                res.append("\n");
        }
        return res.toString();
    }

    public void addBiDirectionalEdge(int u, int v) {
        this.edges[u].add(v);
        this.edges[v].add(u);
    }

    public void addDirectionalEdge(int u, int v) {
        this.edges[u].add(v);
    }
}
