package org.projectwally.algoritmes.tsp.Dijkstra;

public class Edge
{
    private final Node SOURCE;
    private final Node DESTINATION;
    private final int WEIGHT;

    // Initialize the edge
    public Edge(Node source, Node destination, int weight)
    {
        this.SOURCE = source;
        this.DESTINATION = destination;
        this.WEIGHT = weight;
    }


    //Getters
    public Node getSOURCE(){ return SOURCE; }

    public Node getDESTINATION(){ return DESTINATION; }

    public int getWEIGHT() { return WEIGHT; }


}
