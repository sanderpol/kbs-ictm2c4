package org.projectwally.algoritmes.tsp.Dijkstra;

import lombok.Value;

@Value
public class Node
{
    private int index;

    public Node(int index)
    {
        this.index = index;
    }
}
