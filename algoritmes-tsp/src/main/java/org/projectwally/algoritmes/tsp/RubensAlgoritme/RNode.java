package org.projectwally.algoritmes.tsp.RubensAlgoritme;

public class RNode
{
    public int x;
    public int y;

    private RNode cross;
    private RNode up;
    private RNode hz;

    public RNode(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public void setCrossNode(RNode node)
    {
        this.cross = node;
        this.cross.x = this.x + 1;
        this.cross.y = this.y + 1;
    }

}
