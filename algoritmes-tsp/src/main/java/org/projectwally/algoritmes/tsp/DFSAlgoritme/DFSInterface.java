package org.projectwally.algoritmes.tsp.DFSAlgoritme;

import org.projectwally.algoritmes.tsp.Node;

import java.util.ArrayList;

public interface DFSInterface  {

    void DFSgetNode(ArrayList<Node> nodes);

    void setNodes(ArrayList<Node> nodes);

    void setEdges(ArrayList<Integer>[] edges);

    void initReturnNodes();

    ArrayList<Node> getReturnNodes();

    void setReturnNodesNull();
}
