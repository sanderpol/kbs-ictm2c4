package org.projectwally.algoritmes.tsp.Dijkstra;

import java.util.*;


public class Dijkstra
{
    private final List<Node> NODES;
    private final List<Edge> EDGES;


    /*
    Dijkstra places all nodes into two separate sets: unsettled and settled.
    At first, all nodes are in the unsettled set. Meaning they must be still evaluated.
    A node is moved to the settled set if a shortest path from the source to this node has been found.
    */

    private Set<Node> settledNodes;
    private Set<Node> unsettledNodes;

    private Map<Node, Node> predecessors;
    private Map<Node, Integer> distance;


    public Dijkstra(Graph graph)
    {
        this.NODES = graph.getNodes();
        this.EDGES = graph.getEdges();
    }

    public void execute(Node source)
    {

        //Initialize empty set for the settled- and unsettled nodes
        settledNodes = new HashSet<Node>();
        unsettledNodes = new HashSet<Node>();

        distance = new HashMap<Node, Integer>();
        predecessors = new HashMap<Node, Node>();

        // Distance to source is 0
        distance.put(source, 0);
        unsettledNodes.add(source);

        //While there are still unsettled nodes to settle
        while(unsettledNodes.size() > 0)
        {
            //Get the node that is the easiest to get to
            Node node = getMinimum(unsettledNodes);

            //Move the current node from the unsettled nodes to the settled nodes
            settledNodes.add(node);
            unsettledNodes.remove(node);

            findMinimalDistances(node);
        }
    }

    //Find the minimal distance from a node to its neighbours
    public void findMinimalDistances(Node node)
    {
        //Create list of neighbours for the node
        List<Node> adjacentNodes = getNeighbours(node);

        //Loop over all the adjacent nodes
        for(Node destination : adjacentNodes)
        {
            //If the new route is quicker than an old route
            if(getShortestDistance(destination) > getShortestDistance(node) + getDistance(node, destination))
            {
                //Put the new distance in the distance map
                distance.put(destination, getShortestDistance(node) + getDistance(node, destination));

                //Put the link in the predecessors map to retrace the route later
                predecessors.put(destination, node);

                //Put the destination in the unsettled nodes list, to settle later
                unsettledNodes.add(destination);
            }
        }
    }

    public List<Node> getNeighbours(Node node)
    {

        //Initialize an arraylist of the neighbours
        List<Node> neighbours = new ArrayList<Node>();

        // Loop over all the edges
        for(Edge edge: EDGES)
        {
            //If the edge has the node as the source and an unsettled node as destination,
            // add the destination-node to the neighbours list
            if(edge.getSOURCE().equals(node) && !isSettled(edge.getDESTINATION()))
            {
                neighbours.add(edge.getDESTINATION());
            }
        }

        return neighbours;
    }

    //Checks if a node is in the settled list
    public boolean isSettled(Node node)
    {
        if(settledNodes.contains(node))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public Node getMinimum(Set<Node> nodes)
    {
        Node minimum = null;

        // Loop over all the nodes
        for(Node node: nodes)
        {
            //For the first loop, put the first node as the minimum.
            if(minimum == null)
            {
                minimum = node;
            } // If the new node is closer than the current closest node
            else if(getShortestDistance(node) < getShortestDistance(minimum))
            {
                //Set the new minimum node
                minimum = node;
            }
        }

        return minimum;
    }

    //Get the distance between two nodes
    public int getDistance(Node source, Node destination)
    {
        for(Edge edge : EDGES)
        {
            if(edge.getSOURCE().equals(source) && edge.getDESTINATION().equals(destination))
            {
                return edge.getWEIGHT();
            }
        }

        throw new RuntimeException("Code should not reach here");
    }

    public int getShortestDistance(Node destination)
    {
        //Get the shortest distance to the destination node
        Integer dist = distance.get(destination);

        //If the shortest distance is unknown, return the highest number possible (representing infinity)
        if(dist == null)
        {
            return Integer.MAX_VALUE;
        }
        else
        {
            return dist;
        }
    }

    public LinkedList<Node> getPath(Node destination)
    {
        LinkedList<Node> path = new LinkedList<Node>();

        Node step = destination;

        // If the route isn't possible, return null
        if(predecessors.get(destination) == null)
        {
            return null;
        }

        path.add(destination);

        //Loop over all the predecessors
        while(predecessors.get(step) != null)
        {
            step = predecessors.get(step);
            path.add(step);
        }

        // Put the path in the right order
        Collections.reverse(path);

        return path;
    }
}
