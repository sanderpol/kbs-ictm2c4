package org.projectwally.algoritmes.tsp.Dijkstra;

import java.util.List;

public class Graph
{
    private final List<Node> NODES;
    private final List<Edge> EDGES;

    // Initialize the graph
    public Graph(List<Node> nodes, List<Edge> edges)
    {
        this.NODES = nodes;
        this.EDGES = edges;
    }


    // Getters
    public List<Node> getNodes(){ return NODES; }

    public List<Edge> getEdges(){ return EDGES; }



}
