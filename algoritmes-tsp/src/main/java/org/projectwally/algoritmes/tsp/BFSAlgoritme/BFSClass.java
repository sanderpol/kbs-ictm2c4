package org.projectwally.algoritmes.tsp.BFSAlgoritme;

import org.projectwally.algoritmes.tsp.Graph;
import org.projectwally.algoritmes.tsp.GraphImporter;
import org.projectwally.algoritmes.tsp.Node;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class BFSClass implements BFSInterface {

    private ArrayList<Node> nodes;
    private ArrayList<Integer>[] edges;
    private ArrayList<Node> returnNodes;

    public void setNodes(ArrayList<Node> nodes) {
        this.nodes = nodes;
    }

    public void setEdges(ArrayList<Integer>[] edges) {
        this.edges = edges;
    }

    public void initReturnNodes() {
        if (this.returnNodes == null) {
            this.returnNodes = new ArrayList<>();
        }
    }

    /**
     * This function traversals through the graph using a Breadth first search algorithm.
     * the idea of this algorithm is that it first searches in the adjacent nodes of the start node.
     * and after that, the algorithm follows up to the next layer of adjacent nodes.
     * till it has found one of the @targetNodes, if so then one target nodes is set to true,
     * and gets added to the return array.
     *
     * @param targetNodes is the array of nodes that is being searched for.
     */
    public void BFSgetNode(ArrayList<Node> targetNodes) {


        //Boolean for all the nodes in the graph, if visited set true.
        boolean[] visited = new boolean[GraphImporter.NR_NODES];
        boolean[] targetNodesVisited = new boolean[targetNodes.size()];
        //Queue for BFS
        LinkedList<Integer> queue = new LinkedList<>();

        // set currentNode as START_NODE
        int currentNode = GraphImporter.START_NODE;

        //Mark Start node as visited and add to queue.
        visited[currentNode] = true;
        queue.add(currentNode);

        while (queue.size() != 0) {

            //Dequeue the node from the queue
            currentNode = queue.poll();

            // walks through the adjacent nodes and looks if these nodes have been visited.
            for (int n : edges[currentNode]) {
                if (!visited[n]) {

                    // if n is the same node as the searchnode, then all boolean are set to true and the node is
                    // assigned as return node
                    for (int j = 0; j < targetNodesVisited.length; j++) {
                        if (nodes.get(n).getValue() == targetNodes.get(j).getValue() && !targetNodesVisited[j]) {
                            targetNodesVisited[j] = true;
                            Arrays.fill(visited, false);
                            this.returnNodes.add(nodes.get(n));
                            if (areAllTrue(targetNodesVisited)) {
                                Arrays.fill(visited, true);
                            }
                            queue.clear();
                        }
                    }
                    visited[n] = true;
                    queue.add(n);
                }
            }
        }

    }


    /**
     * search through the array, if one of the values equals false then return a false value
     *
     * @param array boolean [] with true or false values
     * @return True OR False
     */
    public static boolean areAllTrue(boolean[] array) {
        for (boolean b : array) {
            if (!b) return false;
        }
        return true;
    }


    public ArrayList<Node> getReturnNodes() {
        return this.returnNodes;
    }

}
