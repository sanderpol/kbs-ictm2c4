package org.projectwally.algoritmes;

import org.junit.Assert;
import org.junit.Test;
import org.projectwally.algoritmes.tsp.DFSAlgoritme.DFSClass;
import org.projectwally.algoritmes.tsp.DFSAlgoritme.DFSInterface;
import org.projectwally.algoritmes.tsp.Graph;
import org.projectwally.algoritmes.tsp.GraphImporter;
import org.projectwally.algoritmes.tsp.Node;

import java.lang.management.ManagementFactory;
import java.util.ArrayList;

public class DFSTest {

    private final static DFSInterface DFS_INTERFACE = new DFSClass();
    public final static int[] SEARCH_NODE = {1333, 1246, 1, 478, 833, 458};
    public final static int[] SEARCH_VALUES = {649325, 875286, 486851, 254832, 549843, 694696};
    private final static ArrayList<Node> SEARCH_NODES = new ArrayList<>();
    public final static int SEARCH_TIMES = 1000;

    /**
     * In this test script we calculate the average time for a graph traversal with visiting all nodes.
     * During this test we run a timer to count the duration of a average traversal.
     *
     * @throws Exception
     */
    @Test
    public void DFSTestRun() throws Exception {

        // Check if we can measure the CPU usage time with JMX
        // If we would do this with System.nanoTime() it would be inaccurate because
        // other processes on the host computer can cause interferance
        Assert.assertTrue(ManagementFactory.getThreadMXBean().isCurrentThreadCpuTimeSupported());

        Graph graph = GraphImporter.getGraph();
        DFS_INTERFACE.setNodes(graph.getNodes());
        DFS_INTERFACE.setEdges(graph.getEdges());
        DFS_INTERFACE.initReturnNodes();

        for (Node n : SEARCH_NODES) {
            graph.setNode(n);
        }

        // Register the start time
        long startTime = ManagementFactory.getThreadMXBean().getCurrentThreadCpuTime();

        for (int i = 0; i < SEARCH_TIMES; i++) {
            DFS_INTERFACE.DFSgetNode(SEARCH_NODES);

        }
        // Calculate the time it took to run
        long deltaTime = ManagementFactory.getThreadMXBean().getCurrentThreadCpuTime() - startTime;

        ArrayList<Node> returnNodes = DFS_INTERFACE.getReturnNodes();
        ArrayList<Node> outputReturnNodes = new ArrayList<>();

        for (int i = 0; i < 6; i++) {
            outputReturnNodes.add(returnNodes.get(i));
        }

        for (Node node : outputReturnNodes
        ) {
            System.out.println(node.getIndex());

        }

        System.out.println("DFS calculation time was " + deltaTime / SEARCH_TIMES + " nanoseconds");

    }

    static {
        for (int i = 0; i < SEARCH_NODE.length; i++) {
            SEARCH_NODES.add(new Node(SEARCH_NODE[i], SEARCH_VALUES[i]));
        }
    }
}
