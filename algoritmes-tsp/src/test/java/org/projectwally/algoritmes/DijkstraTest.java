package org.projectwally.algoritmes;

import org.junit.Test;
import org.projectwally.algoritmes.tsp.Dijkstra.Dijkstra;
import org.projectwally.algoritmes.tsp.Dijkstra.Edge;
import org.projectwally.algoritmes.tsp.Dijkstra.Graph;
import org.projectwally.algoritmes.tsp.Dijkstra.Node;

import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class DijkstraTest
{
    public final static int SEARCH_TIMES = 1000;

    private List<Node> nodes;
    private List<Edge> edges;

    @Test
    public void execute()
    {
        // Check if we can measure the CPU usage time with JMX
        // If we would do this with System.nanoTime() it would be inaccurate because
        // other processes on the host computer can cause interferance
        assertTrue(ManagementFactory.getThreadMXBean().isCurrentThreadCpuTimeSupported());

        nodes = new ArrayList<Node>();
        edges = new ArrayList<Edge>();

        //Add the 25 nodes into the nodes array
        for(int i = 0; i < 26; i++)
        {
            Node node = new Node(i);
            nodes.add(node);
        }

        for(int i = 1; i < 9; i++){ addEdge(i, i + 1, 1); }

        for(int i = 10; i < 16; i++){ addEdge(i, i + 1, 1); }

        for(int i = 17; i < 21; i++){ addEdge(i, i + 1, 1); }

        for(int i = 22; i < 24; i++){ addEdge(i, i + 1, 1); }

        addEdge(0, 5, 1);
        addEdge(5, 13, 1);
        addEdge(13, 19, 1);
        addEdge(19, 23, 1);
        addEdge(23, 25, 1);

        Graph graph = new Graph(nodes, edges);

        // Register the start time
        long startTime = ManagementFactory.getThreadMXBean().getCurrentThreadCpuTime();

        Dijkstra algo = new Dijkstra(graph);

        for (int i = 0; i < SEARCH_TIMES-1; i++) {
            algo.execute(nodes.get(0));
        }

        algo.execute(nodes.get(0));

        //Get the path to node 10
        LinkedList<Node> route = algo.getPath(nodes.get(24));

        // Calculate the time it took to run
        long deltaTime = ManagementFactory.getThreadMXBean().getCurrentThreadCpuTime() - startTime;

        //Test conditions
        assertFalse(route.size() == 0);
        assertNotNull(route);

        for(Node node: route)
        {
            System.out.println(node.getIndex());
        }

        System.out.println("Dijkstra average calculation time was " + deltaTime / SEARCH_TIMES + " nanoseconds");
    }




    //Add an edge to and from a certain node, and with a certain weight
    public void addEdge(int source, int destination, int weight)
    {
        Edge edge = new Edge(nodes.get(source), nodes.get(destination), weight);
        Edge edge2 = new Edge(nodes.get(destination), nodes.get(source), weight);
        edges.add(edge);
        edges.add(edge2);
    }

}
